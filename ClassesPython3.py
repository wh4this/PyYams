# -*- coding: utf-8 -*-
from __future__ import print_function  # Le print de Python 3 dans python 2 avec la librairie future
import random

"""
    Author : FAIVRE Mathis (mathis.faivre@outlook.com)
    Classes utilisées pour faire fonctionner le jeu du Yams
"""


class De(object):
    """Classe dé"""

    def __init__(self):
        """Quand on créer un dé, il est déjà lancé"""
        self.face = random.randint(1, 6)

    def lancer(self):
        """Méthode qui donne une nouvelle valeur au dé"""
        self.face = random.randint(1, 6)

    def getface(self):
        """Méthode qui retourne la face d'un dé"""
        return self.face

class Combinaisons(object):
    """Classe Combinaisons qui permet de calculer la valeur de chaque combinaison"""

    def __init__(self):
        """Un dictionnaire de combinaison et de fonction en attribut"""
        self.combi_dico = {
            '1': self.nombre,
            '2': self.nombre,
            '3': self.nombre,
            '4': self.nombre,
            '5': self.nombre,
            '6': self.nombre,
            'brelan': self.brelan,
            'carre': self.carre,
            'full': self.full_combi,
            'psuite': self.psuite,
            'gsuite': self.gsuite,
            'yams': self.yams,
            'chance': self.chance
        }

    def retcombi(self, combi):
        """Retourne la fonction qui correspond à la combinaison en paramètre"""
        return self.combi_dico[combi]

    def plus_commun(self, liste):
        """Méthode permettant de trouver l'élément le plus commun dans une liste"""
        return max(set(liste), key=liste.count)

    def nombre(self, faces, x):
        """Méthode qui permet de calculer la valeur de la combinaison choisie si c'est un nombre"""
        if x in faces :
            return (faces.count(x) * x)
        else :
            return 0

    def brelan(self, faces):
        """Méthode qui retourne la valeur de la combinaison choisie
        si il y a plus de 3 éléments similaires ou plus"""
        x = self.plus_commun(faces)
        if faces.count(x) >= 3:
            return x * 3
        else :
            return 0

    def full_combi(self, faces):
        """Méthode qui détecte la face la plus commune et la seconde face plus commune
        Si elle sont respectivement 2 et 3, alors le score retourné est 25, sinon 0"""
        liste_temp = faces
        x = self.plus_commun(liste_temp)
        if (liste_temp.count(x) == 3):
            # On supprime l'élément le plus commun pour pouvoir trouver le deuxième plus commun
            filter(lambda a: a != x, liste_temp)
            x = self.plus_commun(liste_temp)
            if liste_temp.count(x) == 2:
                return 25
            else:
                return 0
        else:
            return 0

    def carre(self, faces):
        """Méthode qui retourne la valeur de la combinaison choisie
        si il y a plus de 4 éléments similaires ou plus"""
        x = self.plus_commun(faces)
        if faces.count(x) >= 4 :
            return x * 4
        else :
            return 0

    def gsuite(self, faces):
        """Méthode qui retourne la valeur de la combinaison choisie
        si il y a une suite de 5 éléments dans la liste"""
        liste_temp = faces
        liste_temp.sort()
        if (liste_temp[0]==1 and liste_temp[1]==2 and liste_temp[2]==3 and liste_temp[3]==4 and liste_temp[4]==5) or (liste_temp[0]==2 and liste_temp[1]==3 and liste_temp[2]==4 and liste_temp[3]==5 and liste_temp[4]==6) :
            return 40
        else :
            return 0

    def psuite(self, faces):
        """Méthode qui retourne la valeur de la combinaison choisie
        si il y a une suite de 4 éléments dans la liste"""
        liste_temp = faces
        liste_temp.sort()
        if (liste_temp[0]==1 and liste_temp[1]==2 and liste_temp[2]==3 and liste_temp[3]==4) or (liste_temp[0]==2 and liste_temp[1]==3 and liste_temp[2]==4 and liste_temp[3]==5) or (liste_temp[0]==3 and liste_temp[1]==4 and liste_temp[2]==5 and liste_temp[3]==6) or (liste_temp[1]==2 and liste_temp[2]==3 and liste_temp[3]==4 and liste_temp[4]==5) or (liste_temp[1]==2 and liste_temp[2]==3 and liste_temp[3]==4 and liste_temp[4]==5) or (liste_temp[1]==2 and liste_temp[2]==3 and liste_temp[3]==4 and liste_temp[4]==5) :
            return 30
        else :
            return 0

    def yams(self, faces):
        """Méthode qui retourne la valeur de la combinaison choisie
        si il y a 5 éléments similaires"""
        x = self.plus_commun(faces)
        if faces.count(x) == 5 :
            return 50
        else :
            return 0

    def chance(self, faces):
        """Méthode qui calcul la sommes des éléments de la liste"""
        return faces[0] + faces[1] + faces[2] + faces[3] + faces[4]


class Joueur(object):
    """Classe permettant de créer un joueur"""

    def __init__(self, pseudo):
        """Quand on créer un joueur, il a un pseudo"""
        self.pseudo = pseudo


class CPU(object):
    """Classe CPU qui hérite de la classe joueur(C'est un joueur avec le pseudo 'Ordinateur')"""

    def jouer(self, fichedescore):
        while len(fichedescore.combi_dispo) > 0:
            aselectionne = False
            nbrelances = 2
            faces = fichedescore.getfaces()
            cpts = [faces.count(1), faces.count(2), faces.count(3), faces.count(4), faces.count(5), faces.count(6)]
            while nbrelances > 0:
                if 5 in cpts and 'yams' in fichedescore.combi_dispo:
                    fichedescore.selectionner_ia('yams')
                    aselectionne = True
                    print(cpts)
                    break
                elif cpts == [1, 1, 1, 1, 1, 0] or cpts == [0, 1, 1, 1, 1, 1] and 'gsuite' in fichedescore.combi_dispo:
                    fichedescore.selectionner_ia('gsuite')
                    aselectionne = True
                    print(cpts)
                    break
                elif cpts == [1, 1, 1, 1, 0, 0] \
                        or cpts == [0, 0, 1, 1, 1, 1] \
                        or cpts == [0, 1, 1, 1, 1, 0] \
                        or cpts == [0, 0, 1, 1, 1, 1] \
                        or cpts == [1, 1, 1, 1, 1, 0] \
                        or cpts == [0, 1, 1, 1, 1, 1] and 'psuite' in fichedescore.combi_dispo:
                    fichedescore.selectionner_ia('psuite')
                    aselectionne = True
                    print(cpts)
                    break
                elif 3 in cpts and 2 in cpts and 'full' in fichedescore.combi_dispo:
                    fichedescore.selectionner_ia('full')
                    aselectionne = True
                    print(cpts)
                    break
                elif 4 in cpts or 5 in cpts and 'carre' in fichedescore.combi_dispo:
                    fichedescore.selectionner_ia('carre')
                    aselectionne = True
                    print(cpts)
                    break
                elif 3 in cpts or 4 in cpts or 5 in cpts and 'brelan' in fichedescore.combi_dispo:
                    fichedescore.selectionner_ia('brelan')
                    aselectionne = True
                    print(cpts)
                    break
                elif cpts[5] > 0 and '6' in fichedescore.combi_dispo:
                    fichedescore.selectionner_ia('6')
                    aselectionne = True
                    print(cpts)
                    break
                elif cpts[4] > 0 and '5' in fichedescore.combi_dispo:
                    fichedescore.selectionner_ia('5')
                    aselectionne = True
                    print(cpts)
                    break
                elif cpts[3] > 0 and '4' in fichedescore.combi_dispo:
                    fichedescore.selectionner_ia('4')
                    aselectionne = True
                    print(cpts)
                    break
                elif cpts[2] > 0 and '3' in fichedescore.combi_dispo:
                    fichedescore.selectionner_ia('3')
                    aselectionne = True
                    print(cpts)
                    break
                elif cpts[1] > 0 and '2' in fichedescore.combi_dispo:
                    fichedescore.selectionner_ia('2')
                    aselectionne = True
                    print(cpts)
                    break
                elif cpts[0] > 0 and '1' in fichedescore.combi_dispo:
                    fichedescore.selectionner_ia('1')
                    aselectionne = True
                    print(cpts)
                    break
                elif 'chance' in fichedescore.combi_dispo:
                    fichedescore.selectionner_ia('chance')
                    aselectionne = True
                    print(cpts)
                    break
                for de in fichedescore.des:
                    De.lancer(de)
                faces = fichedescore.getfaces()
                cpts = [faces.count(1), faces.count(2), faces.count(3), faces.count(4), faces.count(5), faces.count(6)]
                nbrelances -= 1
            if aselectionne is False:
                fichedescore.selectionner_ia(random.choice(fichedescore.combi_dispo))
                print(cpts)
            for de in fichedescore.des:
                De.lancer(de)
        fichedescore.scoretotal()
        print('Le score de l\'ordinateur est (pour chaque combinaison)', fichedescore.combi_choisis)
        print('SCORE TOTAL DE ORDINATEUR :', fichedescore.score)
    
    '''##################
    # Methodes de test pour trouver la meilleur ia
    def jouer_avec_methode(self, fichedescore):
        while len(fichedescore.combi_dispo) > 0:
            faces = fichedescore.getfaces()
            print(faces)
            fichedescore.selectionner_ia(self.meilleur_choix(fichedescore.combi_dispo, faces))
            map(lambda d: d.lancer(), fichedescore.des)
        fichedescore.scoretotal()
        print('Le score de l\'ordinateur est (pour chaque combinaison)', fichedescore.combi_choisis)
        print('SCORE TOTAL DE ORDINATEUR :', fichedescore.score)
    

    @staticmethod
    def meilleur_choix(combi_dispo, faces):
        c = Combinaisons()
        score_possible = {}
        for i in range(len(combi_dispo)):
            score_possible[c.retcombi(combi_dispo[i])] = combi_dispo[i]
        combi = max(score_possible.keys())
        return score_possible[combi]
    
    """@staticmethod
    def getProb(combi_dispo, faces):
        c = Combinaisons()
        score_possible = []
        combi_possible = []
        for i in range(len(combi_dispo)):
            score_possible.append(c.retcombi(combi_dispo[i]))
            combi_possible.append(combi_dispo)
        result = zip(combi_possible, score_possible)

        return combi"""
    #########################################'''

class FichedeScore(object):
    """Classe FichedeScore qui permet de sélectionner une combinaison et qui calcule le score du joueur"""

    def __init__(self, joueur = Joueur('Jean-Marie')):
        """Quand on créer une fiche de score, elle appartient à un joueur, elle possède un score, des combinaisons \
        disponibles, des combinaisons choisis, un score et 5 dés"""
        self.score = 0 # le score total de la fiche, calculé à la complétion d'une fiche
        self.combi_dispo = ['1', # La liste de combinaisons disponible dans la fiche
                            '2',
                            '3',
                            '4',
                            '5',
                            '6',
                            'brelan',
                            'carre',
                            'full',
                            'psuite',
                            'gsuite',
                            'yams',
                            'chance']
        self.combi_choisis = {} # Le dictionnaire de combinaisons choisis (avec la combinaison en key et sa valeur en value)
        self.joueur = joueur # Le joueur de la fiche
        self.des = [De(), De(), De(), De(), De()] # 5 dés par fiche pour éviter de confondre les dés des autre joueurs
        self.c = Combinaisons()

    def getfaces(self):
        """Méthode qui retourne les faces des dés de la fiche"""
        # Création d'une liste vide qui va contenir la valeur des faces des dés
        # return map(lambda d: d.getface(), self.des)
        # map est plus lent (gain de 50µs avec la solution d'en dessous)
        return [d.getface() for d in self.des]
    
    def selectionner(self, combi):
        """Méthode qui séléctionne une combinaison dans la fiche de score"""
        # Création d'une liste faces qui représente les faces des 5 dés du joueur
        faces = self.getfaces()

        # Enregistre la fonction utilisé pour calculer la valeur
        # de la combinaison choisis dans une variable
        lacombi = self.c.retcombi(combi)

        # Si la combinaison choisie est disponible
        if combi in self.combi_dispo:

            # Si la combinaison choisi est de taille 1 (une combinaison simple), on convertit la combinaison
            # en un entier pour calculer sa valeur, sinon on ajoute juste la combinaison et son score au dico
            if len(combi) == 1:
                self.combi_choisis.update({combi: lacombi(faces, int(combi))})
            else:
                self.combi_choisis.update({combi: lacombi(faces)})

            # On supprime la combinaison utilisée des combinaisons disponible
            self.combi_dispo.remove(combi)
            print ('La combinaison a été choisis et le score à été mis à jour')

            # Relancement des dés si la combinaison à été choisie
            for de in self.des:
                De.lancer(de)
            # retourne 1 si la séléction à été fructueuse, sinon 0
            return 1
        else:
            print ('Cette combinaison n\'éxiste pas ou à déjà été utilisé')
            return 0

    
    def selectionner_ia(self, combi):
        """Méthode de séléctionnement simplifié pour les cpu"""
        faces = self.getfaces()
        lacombi = self.c.retcombi(combi)

        if combi in self.combi_dispo:
            # Si la combinaison choisi est de taille 1 (une combinaison simple), on convertit la combinaison
            # en un entier pour calculer sa valeur, sinon on ajoute juste la combinaison et son score au dico
            if len(combi) == 1:
                self.combi_choisis.update({combi: lacombi(faces, int(combi))})
            else:
                self.combi_choisis.update({combi: lacombi(faces)})
            # On supprime la combinaison utilisée des combinaisons disponible
            self.combi_dispo.remove(combi)
            '''map(lambda d: d.lancer(), self.des)'''
            for de in self.des:
                De.lancer(de)
            return 1
        else:
            return 0

    def scoretotal(self):
        """Méthode de retourne le score total actuel"""
        # Le bonus (35 pts) est obtenu quand le score total des combinaisons simples de la fiche sont supérieurs ou égaux à 63
        bonus = self.combi_choisis['1'] + \
                self.combi_choisis['2'] + \
                self.combi_choisis['3'] + \
                self.combi_choisis['4'] + \
                self.combi_choisis['5'] + \
                self.combi_choisis['6']
        if bonus >= 63:
            print('Bonus obtenu !')
            self.score += 35
        # Le score total est calculé grâce au dictionnaire des combinaisons choisis de la fiche (la somme des valeurs)
        valeurs = list(self.combi_choisis.values())
        for i in range(len(valeurs)):
            self.score += valeurs[i]
        return self.score


class Partie(object):
    """Classe Partie qui permet de créer une partie avec un/des joueur(s) et/ou une ia"""
    
    def __init__(self, joueur1 = Joueur('Jean-Marie'), joueur2=None, contrecpu=False):
        """Pour la création d'une partie, il y a au moins un joueur (par défaut), on peut ajouter un autre joueur ou un cpu"""
        self.j1 = joueur1
        self.j2 = joueur2
        self.cpu = contrecpu

    def jouer(self):
        """Méthode qui permet de jouer au jeu en choisissant le mode"""
        rejouer = 1
        while rejouer == 1:
            if self.j2 is None and self.cpu is False:
                self.jouerseul()
            elif self.cpu is False:
                self.joueradeux()
            elif self.j2 is None and self.cpu is True:
                self.jouercontrecpu()
            else:
                print('On ne peut pas joueur à deux avec le cpu pour l\'instant car je développe ce jeu avec mes pieds')
                break
            rejouer = int(input('Rejouer ? 1 pour oui et 0 pour non'))
        input('Appuyez sur Entrée pour quitter le jeu')

    def jouerseul(self):
        """Méthode qui définis le déroulement du jeu avec un joueur"""
        f1 = FichedeScore(self.j1)
        while len(f1.combi_dispo) > 0:
            print('####### \nIl y a ', len(f1.combi_dispo), 'combinaison(s) restante(s) dont : ', f1.combi_dispo)
            print('Vos dés : ', f1.getfaces())
            self.demanderelance(f1)
            self.demandecombinaison(f1)
        print('La partie est terminée')
        self.affichescore(f1)

    def joueradeux(self):
        """Méthode qui définis le déroulement du jeu avec deux joueur"""
        f1 = FichedeScore(self.j1)
        f2 = FichedeScore(self.j2)
        while len(f2.combi_dispo) > 0 and len(f1.combi_dispo) > 0:
            print('####### \nTour de ', self.j1.pseudo, ' (le joueur 1) : \n ')
            print('Il y a ', len(f1.combi_dispo), 'combinaison(s) restante(s) dont : ', f1.combi_dispo, '\n')
            print('Vos dés : ', f1.getfaces())
            self.demanderelance(f1)
            self.demandecombinaison(f1)
            print('####### \nTour de ', self.j2.pseudo, ' (le joueur 2) : \n ')
            print('Le joueur 2 possède', len(f2.combi_dispo),
                  'combinaison(s) restante(s) dont : ', f2.combi_dispo, '\n')
            print('Vos dés : ', f2.getfaces())
            self.demanderelance(f2)
            self.demandecombinaison(f2)
            print('#######')
        print('La partie est terminée')
        self.affichescore(f1)
        self.affichescore(f2)
        if f1.score > f2.score:
            print('le joueur ', self.j1.pseudo, 'remporte la partie contre le joueur', self.j2.pseudo)
        else:
            print('le joueur ', self.j2.pseudo, 'remporte la partie contre le joueur', self.j1.pseudo)

    def jouercontrecpu(self):
        """Méthode qui définis le déroulement du jeu avec un joueur et un cpu"""
        cpu1 = CPU()
        f1 = FichedeScore(self.j1)
        f2 = FichedeScore('Ordinateur')
        while len(f1.combi_dispo) > 0:
            print('####### \nIl y a ', len(f1.combi_dispo), 'combinaison(s) restante(s) dont : ', f1.combi_dispo)
            print('Vos dés : ', f1.getfaces())
            self.demanderelance(f1)
            self.demandecombinaison(f1)
        print('La partie est terminée')
        self.affichescore(f1)
        cpu1.jouer(f2)
    
    @staticmethod
    def demanderelance(fichedescore):
        """Méthode qui définis la demande de relance de dés au joueur"""
        nbessais = 2
        while nbessais > 0:
            while True:
                relancer = str(input('Voulez-vous relancer un ou plusieurs dés ? o/n : '))
                try:
                   relancer = str(relancer)
                except ValueError:
                   print('Reponse non valide')
                   continue
                if relancer == 'o' or relancer == 'n':
                   break
                else:
                   print('Reponse non valide')
            if relancer == 'o' :
                print('1 pour relancer et 0 pour garder')
                while True:
                    print('Veuillez écrire avec le format : 1 0 1 0 1')
                    desarelancer = [int(x) for x in input().split()]
                    if len(desarelancer) == 5 :
                        break
                    else:
                       print('Reponse non valide')
                for i in range(5):
                    if desarelancer[i] >= 1:
                        fichedescore.des[i].lancer()
                print(fichedescore.getfaces())
                nbessais -= 1
            else:
                break
        if nbessais == 0:
            print('Vous avez épuisé tout vos essais')

    
    @staticmethod
    def demandecombinaison(fichedescore):
        """Méthode qui définis la demande de combinaison au joueur"""
        while True:
            combinaison = str(input('Quel combinaison souhaitez-vous selectionner ? '))
            try:
                combinaison = str(combinaison)
            except ValueError:
                print('Reponse non valide')
                continue
            if combinaison in fichedescore.combi_dispo :
                break
            else:
                print('Reponse non valide')
        verification = fichedescore.selectionner(combinaison)
        while verification == 0:
            combinaison = str(input('Quel combinaison souhaitez-vous selectionner ? '))
            verification = fichedescore.selectionner(combinaison)
    
    @staticmethod
    def affichescore(fichedescore):
        """Méthode qui définis l'affichage du score du joueur de la fiche"""
        fichedescore.scoretotal()
        print('Le score de', fichedescore.joueur.pseudo, 'est (pour chaque combinaison)', fichedescore.combi_choisis)
        print('SCORE TOTAL DE',fichedescore.joueur.pseudo,':', fichedescore.score)
        return fichedescore.score
